# Repositórios de Helm Charts

O Objetivo desse repositório é a centralização e guarda do Helm CHarts desenvolvidos para aplicações e componentes dos softwares corporativos utilizandos no trinamanto de Kubernetes.

# Estrutura padrão

O Repositório está estruturado da seguinte forma:

***dir
charts
    <Applications charts>
        Chart.yaml
        values.yaml
        *** 
README.md
***
##